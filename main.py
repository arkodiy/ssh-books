import subprocess
import yaml


def load_book(filename="books.yaml"):
    with open(filename, 'r') as file:
        books = yaml.safe_load(file)
        return books


def draw_book(books):
    for i, book in enumerate(books, 1):
        print(f"{i}: {book['name']}")


def execute_connection(book):
    subprocess.run(["ssh", book["address"]])


books_ = load_book()
draw_book(books_)
s_index = int(input("Select number "))
execute_connection(books_[s_index-1])


